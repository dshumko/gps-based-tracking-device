<?php
define('DOC_ROOT', realpath(dirname(__FILE__) . '/../'));
define('SITE_NAME', substr(DOC_ROOT, strlen(realpath($_SERVER['DOCUMENT_ROOT']))) . '/');

/**
 * Automatically includes classes
 * 
 * @throws Exception
 * 
 * @param  string $class_name  Name of the class to load
 * @return void
 */
 fAuthorization::setLoginPage(SITE_NAME . 'index.php');

 fAuthorization::setAuthLevels(
    array(
        'admin' => 100,
        'user'  => 50
    )
);

function __autoload($class_name)
{
    // Customize this to your root Flourish directory
    //$flourish_root = './lib/';
    $flourish_root = DOC_ROOT . '/php/lib/';
    
    $file = $flourish_root . $class_name . '.php';
 
    if (file_exists($file)) {
        include $file;
        return;
    }
    
    throw new Exception('The class ' . $class_name . ' could not be loaded');
}